package com.example.rajassginment.common

object AppConstant {
    const val APP_SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val REPOSITORY_CREATED_AT_DATE_FORMAT = "dd MMM yyy, hh:mm a"
}