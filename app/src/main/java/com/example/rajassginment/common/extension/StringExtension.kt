package com.example.rajassginment.common.extension

import com.example.rajassginment.common.utills.createAtDateFormatter
import com.example.rajassginment.common.utills.serverDateFormatter
import com.example.rajassginment.common.utills.utcCalendar
import timber.log.Timber

fun String.convertServerDateToAppDateString(): String =
    run {
        try {
            serverDateFormatter.parse(this)?.let { utcCalendar.time = it }
        } catch (e: Exception) {
            Timber.e(e)
        }
        createAtDateFormatter.format(utcCalendar.timeInMillis)
    }