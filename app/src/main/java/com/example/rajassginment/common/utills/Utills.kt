package com.example.rajassginment.common.utills

import com.example.rajassginment.common.AppConstant
import java.text.SimpleDateFormat
import java.util.*

var createAtDateFormatter =
    SimpleDateFormat(AppConstant.REPOSITORY_CREATED_AT_DATE_FORMAT, Locale.getDefault()).apply {
        timeZone = TimeZone.getDefault()
    }

var serverDateFormatter =
    SimpleDateFormat(AppConstant.APP_SERVER_DATE_FORMAT, Locale.getDefault()).apply {
        timeZone = TimeZone.getTimeZone("UTC")
    }

val utcCalendar: Calendar = Calendar.getInstance().apply { timeZone = TimeZone.getTimeZone("UTC") }