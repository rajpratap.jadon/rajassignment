package com.example.rajassginment.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.rajassginment.databinding.CustomSnackbarLayoutBinding
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment<V : ViewBinding>(@LayoutRes private val layOutId: Int) :
    Fragment() {

    lateinit var fragmentBinding: V

    private lateinit var snackBar: Snackbar
    private lateinit var layout: Snackbar.SnackbarLayout
    private lateinit var customSnackBarLayoutBinding: CustomSnackbarLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setObserver()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentBinding = DataBindingUtil.inflate(inflater, layOutId, container, false)
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createSnackBar()
    }

    override fun onStart() {
        super.onStart()
        setUpBindingVariables()
        setClickListener()
    }

    private fun createSnackBar() {
        snackBar = Snackbar.make(requireView(), "", Snackbar.LENGTH_LONG)
        layout = snackBar.view as Snackbar.SnackbarLayout

        customSnackBarLayoutBinding =
            CustomSnackbarLayoutBinding.inflate(LayoutInflater.from(requireContext()))

        layout.addView(customSnackBarLayoutBinding.root, 0)
    }

    fun showSnackBar(message: String) {
        customSnackBarLayoutBinding.snackBarMessage.text = message
        snackBar.show()
    }

    abstract fun setObserver()
    abstract fun setUpBindingVariables()
    abstract fun setClickListener()

    override fun onDestroyView() {
        super.onDestroyView()
        viewModelStore.clear()
    }
}