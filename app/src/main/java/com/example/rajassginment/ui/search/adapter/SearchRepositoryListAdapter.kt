package com.example.rajassginment.ui.search.adapter

import `in`.primathon.lib_core.baseAdapter.BaseAdapter
import com.example.rajassginment.R
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import com.example.rajassginment.databinding.RepositoryItemBinding

class SearchRepositoryListAdapter :
    BaseAdapter<RepositoryItemDomainModel, RepositoryItemBinding>() {
    private lateinit var bookMarkedRepositoryId: List<Int>

    fun setRepositoryId(bookMarkedRepositoryId: List<Int>) {
        this.bookMarkedRepositoryId = bookMarkedRepositoryId
    }

    override fun bind(
        viewBinding: RepositoryItemBinding,
        item: RepositoryItemDomainModel,
        position: Int
    ) {

        viewBinding.repositoryItem = item

        val isBookMarked =
            this::bookMarkedRepositoryId.isInitialized && bookMarkedRepositoryId.contains(item.repositoryId)

        viewBinding.isBookMarked = isBookMarked

        viewBinding.repoBookmark.setOnClickListener {
            if (isBookMarked.not())
                listener(it, item, position)
        }
    }

    override fun getItemViewType(position: Int) = R.layout.repository_item
}