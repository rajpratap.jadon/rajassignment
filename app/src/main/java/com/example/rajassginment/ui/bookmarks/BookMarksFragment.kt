package com.example.rajassginment.ui.bookmarks

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.example.rajassginment.R
import com.example.rajassginment.common.BaseFragment
import com.example.rajassginment.databinding.FragmentBookMarksBinding
import com.example.rajassginment.ui.bookmarks.adapter.BookMarksRepositoryAdapter
import com.example.rajassginment.ui.bookmarks.viewmodel.BookMarksFragmentViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class BookMarksFragment : BaseFragment<FragmentBookMarksBinding>(R.layout.fragment_book_marks) {

    private val bookMarksFragmentViewModel by activityViewModels<BookMarksFragmentViewModel>()
    private val adapter = BookMarksRepositoryAdapter()

    override fun setObserver() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                bookMarksFragmentViewModel.getBookMarkedRepositories().collect {
                    adapter.submitList(it)
                    fragmentBinding.isData = adapter.itemCount > 0
                }
            }
        }
    }

    override fun setUpBindingVariables() {
        fragmentBinding.adapter = adapter
    }

    override fun setClickListener() {
        adapter.listener = { view, item, _ ->
            when (view.id) {
                R.id.repoBookmark -> bookMarksFragmentViewModel.deleteBookMarkedRepository(item)
                R.id.repoItemRow -> {
                    val sendRepositoryId =
                        BookMarksFragmentDirections.actionBookMarksFragmentToBookMarkDetails()
                    sendRepositoryId.repositoryId = item.repositoryId
                    findNavController().navigate(sendRepositoryId)
                }
            }
        }
    }
}