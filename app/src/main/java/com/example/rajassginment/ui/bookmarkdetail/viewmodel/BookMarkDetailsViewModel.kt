package com.example.rajassginment.ui.bookmarkdetail.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rajassginment.common.extension.toSharedFlow
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import com.example.rajassginment.data.remote.repo.RepositoryRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookMarkDetailsViewModel @Inject constructor(private val repositoryRepo: RepositoryRepo) :
    ViewModel() {

    private val _repositoryItem = MutableSharedFlow<RepositoryItemDomainModel>()

    val repositoryItem = _repositoryItem.toSharedFlow()

    fun getRepositoryById(repositoryId: Int) {
        viewModelScope.launch {
            _repositoryItem.emit(repositoryRepo.getBookMarkedRepository(repositoryId))
        }
    }
}