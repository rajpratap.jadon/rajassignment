package com.example.rajassginment.ui.bookmarks.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import com.example.rajassginment.data.remote.repo.RepositoryRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookMarksFragmentViewModel @Inject constructor(private val repositoryRepo: RepositoryRepo) :
    ViewModel() {

    fun getBookMarkedRepositories() = repositoryRepo.getBookMarkedRepositories()

    fun deleteBookMarkedRepository(repository: RepositoryItemDomainModel) {
        viewModelScope.launch { repositoryRepo.deleteBookMarkedRepository(repository) }
    }
}