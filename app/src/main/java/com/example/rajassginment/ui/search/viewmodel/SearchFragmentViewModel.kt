package com.example.rajassginment.ui.search.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rajassginment.common.extension.toSharedFlow
import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.DataState
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import com.example.rajassginment.data.remote.repo.RepositoryRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchFragmentViewModel @Inject constructor(
    private val repositoryRepo: RepositoryRepo
) :
    ViewModel() {

    private val _repositoryItem =
        MutableSharedFlow<DataState<BaseResponse<List<RepositoryItemDomainModel>>>>()

    val repositoryItem = _repositoryItem.toSharedFlow()

    fun searchRepository(query: String) {
        repositoryRepo.getRepositories(query).onEach {
            _repositoryItem.emit(it)
        }.launchIn(viewModelScope)
    }

    fun bookMarkedRepo(repositoryItemDomainModel: RepositoryItemDomainModel) {
        viewModelScope.launch {
            repositoryRepo.bookMarkedRepository(repositoryItemDomainModel)
        }
    }

    fun getBookMarkedRepositoriesId() = repositoryRepo.getBookMarkedRepositoriesId()
}