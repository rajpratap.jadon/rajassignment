package com.example.rajassginment.ui.bookmarks.adapter

import `in`.primathon.lib_core.baseAdapter.BaseAdapter
import com.example.rajassginment.R
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import com.example.rajassginment.databinding.RepositoryItemBinding

class BookMarksRepositoryAdapter :
    BaseAdapter<RepositoryItemDomainModel, RepositoryItemBinding>() {

    override fun getItemViewType(position: Int) = R.layout.repository_item

    override fun bind(
        viewBinding: RepositoryItemBinding,
        item: RepositoryItemDomainModel,
        position: Int
    ) {
        viewBinding.apply {
            repositoryItem = item
            isBookMarked = true

            repoItemRow.setOnClickListener {
                listener(it, item, position)
            }
            repoBookmark.setOnClickListener {
                listener(it, item, position)
            }
        }
    }
}