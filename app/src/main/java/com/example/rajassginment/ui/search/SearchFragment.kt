package com.example.rajassginment.ui.search

import `in`.primathon.lib_core.debounce.DebouncedTextWatcher
import android.text.Editable
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.rajassginment.R
import com.example.rajassginment.common.BaseFragment
import com.example.rajassginment.data.model.DataState
import com.example.rajassginment.databinding.FragmentSearchBinding
import com.example.rajassginment.ui.search.adapter.SearchRepositoryListAdapter
import com.example.rajassginment.ui.search.viewmodel.SearchFragmentViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSearchBinding>(R.layout.fragment_search) {

    private val searchFragmentViewModel by activityViewModels<SearchFragmentViewModel>()
    private val adapter = SearchRepositoryListAdapter()
    private var lastBookMarked = -1

    override fun setObserver() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED)
            {
                launch {
                    searchFragmentViewModel.repositoryItem.collect {
                        when (it) {

                            is DataState.Loading -> fragmentBinding.isLoading = true

                            is DataState.Error -> {
                                fragmentBinding.isLoading = false
                                showSnackBar(it.errorMessage)
                            }

                            is DataState.Success -> {
                                fragmentBinding.isLoading = false
                                adapter.submitList(it.baseResponseData.repositoriesItems)
                                fragmentBinding.isData = adapter.itemCount > 0
                            }
                        }
                    }
                }

                launch {
                    searchFragmentViewModel.getBookMarkedRepositoriesId()
                        .collect {
                            adapter.setRepositoryId(it)
                            adapter.notifyItemChanged(lastBookMarked)
                        }
                }
            }
        }
    }

    override fun setUpBindingVariables() {
        fragmentBinding.adapter = adapter
    }

    override fun setClickListener() {
        fragmentBinding.include.searchEditText.addTextChangedListener(object :
            DebouncedTextWatcher(lifecycle) {
            override fun afterTextDebounced(editable: Editable?) {
                searchFragmentViewModel.searchRepository(editable.toString().trim())
            }
        })

        adapter.listener = { view, item, position ->
            when (view.id) {
                R.id.repoBookmark -> {
                    searchFragmentViewModel.bookMarkedRepo(item)
                    lastBookMarked = position
                }
            }
        }
    }
}