package com.example.rajassginment.ui.bookmarkdetail

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.example.rajassginment.R
import com.example.rajassginment.common.BaseFragment
import com.example.rajassginment.databinding.FragmentBookMarkDetailsBinding
import com.example.rajassginment.ui.bookmarkdetail.viewmodel.BookMarkDetailsViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class BookMarkDetails :
    BaseFragment<FragmentBookMarkDetailsBinding>(R.layout.fragment_book_mark_details) {

    private val bookMarkDetailsViewModel by activityViewModels<BookMarkDetailsViewModel>()

    override fun setObserver() {
        arguments?.let {
            bookMarkDetailsViewModel.getRepositoryById(BookMarkDetailsArgs.fromBundle(it).repositoryId)

            lifecycleScope.launch {
                bookMarkDetailsViewModel.repositoryItem.flowWithLifecycle(
                    lifecycle,
                    Lifecycle.State.STARTED
                ).collect { repositoryItem -> fragmentBinding.repositoryItem = repositoryItem }
            }
        }
    }

    override fun setUpBindingVariables() {
        /*"Not yet implemented"*/
    }

    override fun setClickListener() {
        /*"Not yet implemented"*/
    }
}