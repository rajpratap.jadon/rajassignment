package com.example.rajassginment.data.remote

import com.example.rajassginment.BuildConfig
import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.RepositoryItemEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApiService {

    @GET(BuildConfig.SEARCH_API)
    suspend fun getRepository(@Query(BuildConfig.SEARCH_API_QUERY_PARAMETER) query: String): BaseResponse<List<RepositoryItemEntity>>
}