package com.example.rajassginment.data.datasource.remotedataSource

import com.example.rajassginment.data.mapper.MapperFromEntity
import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.DataState
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import com.example.rajassginment.data.model.RepositoryItemEntity
import com.example.rajassginment.data.remote.SafeApiRequest
import com.example.rajassginment.data.remote.SearchApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor(
    private val searchApiService: SearchApiService,
    private val safeApiRequest: SafeApiRequest,
    private val repositoryItemMapper: MapperFromEntity<BaseResponse<List<RepositoryItemEntity>>, BaseResponse<List<RepositoryItemDomainModel>>>
) : RemoteDataSource {
    override fun getRepositories(query: String): Flow<DataState<BaseResponse<List<RepositoryItemDomainModel>>>> =
        flow {
            emit(DataState.Loading)
            emit(safeApiRequest.apiRequest {
                repositoryItemMapper.mapFromEntity(
                    searchApiService.getRepository(
                        query
                    )
                )
            })
        }
}