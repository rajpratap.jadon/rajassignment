package com.example.rajassginment.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.rajassginment.BuildConfig
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import javax.inject.Singleton

@Singleton
@Database(
    entities = [RepositoryItemDomainModel::class],
    version = BuildConfig.VERSION_CODE,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract val repositoryDAO: RepositoryDAO
}