package com.example.rajassginment.data.model

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.rajassginment.data.remote.repo.NetworkConstant.ARCHIVE
import com.example.rajassginment.data.remote.repo.NetworkConstant.ARCHIVE_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.ASSIGNEES_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.AVATAR_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.BLOB_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.BRANCH_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.CLONE_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.COLLABORATORS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.COMMENTS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.COMMITS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.COMPARE_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.CONTENTS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.CONTRIBUTORS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.CREATED_AT
import com.example.rajassginment.data.remote.repo.NetworkConstant.DEFAULT_BRANCH
import com.example.rajassginment.data.remote.repo.NetworkConstant.DEPLOYMENTS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.DESCRIPTION
import com.example.rajassginment.data.remote.repo.NetworkConstant.DISABLED
import com.example.rajassginment.data.remote.repo.NetworkConstant.DOWNLOADS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.EVENTS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.FOLLOWERS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.FOLLOWING_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.FORK
import com.example.rajassginment.data.remote.repo.NetworkConstant.FORKS
import com.example.rajassginment.data.remote.repo.NetworkConstant.FORKS_COUNT
import com.example.rajassginment.data.remote.repo.NetworkConstant.FORKS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.FULL_NAME
import com.example.rajassginment.data.remote.repo.NetworkConstant.GISTS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.GIT_COMMITS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.GIT_REFS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.GIT_TAG_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.GIT_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.GRAVATAR_ID
import com.example.rajassginment.data.remote.repo.NetworkConstant.HAS_DOWNLOADS
import com.example.rajassginment.data.remote.repo.NetworkConstant.HAS_ISSUES
import com.example.rajassginment.data.remote.repo.NetworkConstant.HAS_PAGES
import com.example.rajassginment.data.remote.repo.NetworkConstant.HAS_PROJECTS
import com.example.rajassginment.data.remote.repo.NetworkConstant.HAS_WIKI
import com.example.rajassginment.data.remote.repo.NetworkConstant.HOMEPAGE
import com.example.rajassginment.data.remote.repo.NetworkConstant.HOOKS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.HTML_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.ISSUE_COMMENT_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.ISSUE_EVENTS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.ISSUE_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.KEY
import com.example.rajassginment.data.remote.repo.NetworkConstant.KEYS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.LABELS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.LANGUAGE
import com.example.rajassginment.data.remote.repo.NetworkConstant.LANGUAGE_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.LICENSE
import com.example.rajassginment.data.remote.repo.NetworkConstant.LOGIN
import com.example.rajassginment.data.remote.repo.NetworkConstant.MASTER_BRANCH
import com.example.rajassginment.data.remote.repo.NetworkConstant.MERGES_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.MILESTONES_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.MIRROR_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.NAME
import com.example.rajassginment.data.remote.repo.NetworkConstant.NODE_ID
import com.example.rajassginment.data.remote.repo.NetworkConstant.NOTIFICATION_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.OPEN_ISSUES
import com.example.rajassginment.data.remote.repo.NetworkConstant.OPEN_ISSUES_COUNT
import com.example.rajassginment.data.remote.repo.NetworkConstant.ORGANIZATION_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.OWNER
import com.example.rajassginment.data.remote.repo.NetworkConstant.PRIVATE
import com.example.rajassginment.data.remote.repo.NetworkConstant.PULLS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.PUSHED_AT
import com.example.rajassginment.data.remote.repo.NetworkConstant.RECEIVED_EVENTS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.RELEASES_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.REPOSITORY_ID
import com.example.rajassginment.data.remote.repo.NetworkConstant.REPO_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.SCORE
import com.example.rajassginment.data.remote.repo.NetworkConstant.SITE_ADMIN
import com.example.rajassginment.data.remote.repo.NetworkConstant.SIZE
import com.example.rajassginment.data.remote.repo.NetworkConstant.SPDX_ID
import com.example.rajassginment.data.remote.repo.NetworkConstant.SSH_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.STARGAZERS_COUNT
import com.example.rajassginment.data.remote.repo.NetworkConstant.STARGAZERS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.STARRED_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.STATUSES_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.SUBSCRIBERS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.SUBSCRIPTIONS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.SUBSCRIPTION_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.SVN_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.TAGS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.TEAMS_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.TREES_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.TYPE
import com.example.rajassginment.data.remote.repo.NetworkConstant.UPDATED_URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.URL
import com.example.rajassginment.data.remote.repo.NetworkConstant.VISIBILITY
import com.example.rajassginment.data.remote.repo.NetworkConstant.WATCHERS
import com.example.rajassginment.data.remote.repo.NetworkConstant.WATCHERS_COUNT
import com.squareup.moshi.Json

@Keep
data class RepositoryItemEntity(
    @Json(name = ARCHIVE_URL)
    var archiveUrl: String?,
    @Json(name = ARCHIVE)
    var archived: Boolean?,
    @Json(name = ASSIGNEES_URL)
    var assigneesUrl: String?,
    @Json(name = BLOB_URL)
    var blobsUrl: String?,
    @Json(name = BRANCH_URL)
    var branchesUrl: String?,
    @Json(name = CLONE_URL)
    var cloneUrl: String?,
    @Json(name = COLLABORATORS_URL)
    var collaboratorsUrl: String?,
    @Json(name = COMMENTS_URL)
    var commentsUrl: String?,
    @Json(name = COMMITS_URL)
    var commitsUrl: String?,
    @Json(name = COMPARE_URL)
    var compareUrl: String?,
    @Json(name = CONTENTS_URL)
    var contentsUrl: String?,
    @Json(name = CONTRIBUTORS_URL)
    var contributorsUrl: String?,
    @Json(name = CREATED_AT)
    var createdAt: String?,
    @Json(name = DEFAULT_BRANCH)
    var defaultBranch: String?,
    @Json(name = DEPLOYMENTS_URL)
    var deploymentsUrl: String?,
    @Json(name = DESCRIPTION)
    var description: String?,
    @Json(name = DISABLED)
    var disabled: Boolean?,
    @Json(name = DOWNLOADS_URL)
    var downloadsUrl: String?,
    @Json(name = EVENTS_URL)
    var eventsUrl: String?,
    @Json(name = FORK)
    var fork: Boolean?,
    @Json(name = FORKS)
    var forks: Int,
    @Json(name = FORKS_COUNT)
    var forksCount: Int,
    @Json(name = FORKS_URL)
    var forksUrl: String?,
    @Json(name = FULL_NAME)
    var fullName: String?,
    @Json(name = GIT_COMMITS_URL)
    var gitCommitsUrl: String?,
    @Json(name = GIT_REFS_URL)
    var gitRefsUrl: String?,
    @Json(name = GIT_TAG_URL)
    var gitTagsUrl: String?,
    @Json(name = GIT_URL)
    var gitUrl: String?,
    @Json(name = HAS_DOWNLOADS)
    var hasDownloads: Boolean?,
    @Json(name = HAS_ISSUES)
    var hasIssues: Boolean?,
    @Json(name = HAS_PAGES)
    var hasPages: Boolean?,
    @Json(name = HAS_PROJECTS)
    var hasProjects: Boolean?,
    @Json(name = HAS_WIKI)
    var hasWiki: Boolean?,
    @Json(name = HOMEPAGE)
    var homepage: String?,
    @Json(name = HOOKS_URL)
    var hooksUrl: String?,
    @Json(name = HTML_URL)
    var htmlUrl: String?,
    @Json(name = REPOSITORY_ID)
    var repositoryId: Int,
    @Json(name = ISSUE_COMMENT_URL)
    var issueCommentUrl: String?,
    @Json(name = ISSUE_EVENTS_URL)
    var issueEventsUrl: String?,
    @Json(name = ISSUE_URL)
    var issuesUrl: String?,
    @Json(name = KEYS_URL)
    var keysUrl: String?,
    @Json(name = LABELS_URL)
    var labelsUrl: String?,
    @Json(name = LANGUAGE)
    var language: String?,
    @Json(name = LANGUAGE_URL)
    var languagesUrl: String?,
    @Json(name = LICENSE)
    var license: License?,
    @Json(name = MASTER_BRANCH)
    var masterBranch: String?,
    @Json(name = MERGES_URL)
    var mergesUrl: String?,
    @Json(name = MILESTONES_URL)
    var milestonesUrl: String?,
    @Json(name = MIRROR_URL)
    var mirrorUrl: String?,
    @Json(name = NAME)
    var name: String?,
    @Json(name = NODE_ID)
    var nodeId: String?,
    @Json(name = NOTIFICATION_URL)
    var notificationsUrl: String?,
    @Json(name = OPEN_ISSUES)
    var openIssues: Int,
    @Json(name = OPEN_ISSUES_COUNT)
    var openIssuesCount: Int,
    @Json(name = OWNER)
    var owner: Owner?,
    @Json(name = PRIVATE)
    var `private`: Boolean?,
    @Json(name = PULLS_URL)
    var pullsUrl: String?,
    @Json(name = PUSHED_AT)
    var pushedAt: String?,
    @Json(name = RELEASES_URL)
    var releasesUrl: String?,
    @Json(name = SCORE)
    var score: Int,
    @Json(name = SIZE)
    var size: Int,
    @Json(name = SSH_URL)
    var sshUrl: String?,
    @Json(name = STARGAZERS_COUNT)
    var stargazersCount: Int,
    @Json(name = STARGAZERS_URL)
    var stargazersUrl: String?,
    @Json(name = STATUSES_URL)
    var statusesUrl: String?,
    @Json(name = SUBSCRIBERS_URL)
    var subscribersUrl: String?,
    @Json(name = SUBSCRIPTION_URL)
    var subscriptionUrl: String?,
    @Json(name = SVN_URL)
    var svnUrl: String?,
    @Json(name = TAGS_URL)
    var tagsUrl: String?,
    @Json(name = TEAMS_URL)
    var teamsUrl: String?,
    @Json(name = TREES_URL)
    var treesUrl: String?,
    @Json(name = UPDATED_URL)
    var updatedAt: String?,
    @Json(name = URL)
    var url: String?,
    @Json(name = VISIBILITY)
    var visibility: String?,
    @Json(name = WATCHERS)
    var watchers: Int,
    @Json(name = WATCHERS_COUNT)
    var watchersCount: Int
)

@Keep
data class License(
    @Json(name = HTML_URL)
    var htmlUrl: String?,
    @Json(name = KEY)
    var key: String?,
    @Json(name = NAME)
    var name: String?,
    @Json(name = NODE_ID)
    var nodeId: String?,
    @Json(name = SPDX_ID)
    var spdxId: String?,
    @Json(name = URL)
    var url: String?
)

@Keep
data class Owner(
    @Json(name = AVATAR_URL)
    var avatarUrl: String?,
    @Json(name = EVENTS_URL)
    var eventsUrl: String?,
    @Json(name = FOLLOWERS_URL)
    var followersUrl: String?,
    @Json(name = FOLLOWING_URL)
    var followingUrl: String?,
    @Json(name = GISTS_URL)
    var gistsUrl: String?,
    @Json(name = GRAVATAR_ID)
    var gravatarId: String?,
    @Json(name = HTML_URL)
    var htmlUrl: String?,
    @Json(name = REPOSITORY_ID)
    var id: Int,
    @Json(name = LOGIN)
    var login: String?,
    @Json(name = NODE_ID)
    var nodeId: String?,
    @Json(name = ORGANIZATION_URL)
    var organizationsUrl: String?,
    @Json(name = RECEIVED_EVENTS_URL)
    var receivedEventsUrl: String?,
    @Json(name = REPO_URL)
    var reposUrl: String?,
    @Json(name = SITE_ADMIN)
    var siteAdmin: Boolean?,
    @Json(name = STARRED_URL)
    var starredUrl: String?,
    @Json(name = SUBSCRIPTIONS_URL)
    var subscriptionsUrl: String?,
    @Json(name = TYPE)
    var type: String?,
    @Json(name = URL)
    var url: String?
)

@Keep
@Entity(tableName = "REPOSITORIES_LOCAL_TABLE")
data class RepositoryItemDomainModel(
    @PrimaryKey @ColumnInfo(name = REPOSITORY_ID)
    var repositoryId: Int = 0,
    @ColumnInfo(name = FULL_NAME)
    var fullName: String = "",
    @ColumnInfo(name = CREATED_AT)
    var createdAt: String = "",
    var isBookMarked: Boolean = false,
)