package com.example.rajassginment.data.datasource.remotedataSource

import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.DataState
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import kotlinx.coroutines.flow.Flow

interface RemoteDataSource {
    fun getRepositories(query: String): Flow<DataState<BaseResponse<List<RepositoryItemDomainModel>>>>
}