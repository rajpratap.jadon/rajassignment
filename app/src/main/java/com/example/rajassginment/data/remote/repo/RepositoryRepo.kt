package com.example.rajassginment.data.remote.repo

import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.DataState
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import kotlinx.coroutines.flow.Flow

interface RepositoryRepo {
    fun getRepositories(query: String): Flow<DataState<BaseResponse<List<RepositoryItemDomainModel>>>>
    suspend fun bookMarkedRepository(repository: RepositoryItemDomainModel)
    fun getBookMarkedRepositoriesId(): Flow<List<Int>>
    suspend fun getBookMarkedRepository(repositoryId: Int): RepositoryItemDomainModel
    fun getBookMarkedRepositories(): Flow<List<RepositoryItemDomainModel>>
    suspend fun deleteBookMarkedRepository(repository: RepositoryItemDomainModel)
}