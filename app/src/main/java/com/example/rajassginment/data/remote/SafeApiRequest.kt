package com.example.rajassginment.data.remote

import com.example.rajassginment.data.model.DataState
import com.example.rajassginment.data.model.ErrorResponse
import com.squareup.moshi.Moshi
import retrofit2.HttpException
import java.net.SocketTimeoutException
import javax.inject.Inject

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1)
}

class SafeApiRequest
@Inject constructor(
    private val networkHelper: NetworkHelper
) {
    suspend fun <T : Any> apiRequest(dataRequest: suspend () -> T): DataState<T> {
        return try {
            if (networkHelper.isNetworkConnected()) {
                DataState.Success(dataRequest.invoke())
            } else
                throw NoInternetException("Please check your Internet Connection")

        } catch (throwable: Throwable) {
            when (throwable) {
                is HttpException -> {
                    val errorResponse = convertErrorBody(throwable)
                    DataState.Error(
                        throwable,
                        errorMessage = errorResponse?.message.toString()
                    )
                }
                is SocketTimeoutException -> DataState.Error(
                    throwable,
                    getErrorMessage(ErrorCodes.SocketTimeOut.code)
                )

                else -> {
                    DataState.Error(throwable as Exception, throwable.message.toString())
                }
            }
        }
    }
}

private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
    return try {
        throwable.response()?.errorBody()?.source()?.let {
            val moshiAdapter = Moshi.Builder().build().adapter(ErrorResponse::class.java)
            moshiAdapter.fromJson(it)
        }
    } catch (exception: Exception) {
        null
    }
}

private fun getErrorMessage(code: Int): String {
    return when (code) {
        ErrorCodes.SocketTimeOut.code -> "Timeout"
        400 -> "Bad Request"
        401 -> "Unauthorised"
        403 -> "Forbidden"
        404 -> "Not found"
        409 -> "Conflict"
        500 -> "Internal Server Error"
        else -> "Something went wrong"
    }
}