package com.example.rajassginment.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BaseResponse<T>(

    @Json(name = "incomplete_results")
    var incompleteResults: Boolean,
    @Json(name = "total_count")
    var totalCount: Int,
    @Json(name = "items")
    var repositoriesItems: T
)