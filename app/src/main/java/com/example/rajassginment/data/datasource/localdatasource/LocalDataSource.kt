package com.example.rajassginment.data.datasource.localdatasource

import com.example.rajassginment.data.model.RepositoryItemDomainModel
import kotlinx.coroutines.flow.Flow

interface LocalDataSource {
    suspend fun bookMarkedRepository(repository: RepositoryItemDomainModel)
    fun getBookMarkedRepositoriesId(): Flow<List<Int>>
    suspend fun getBookMarkedRepository(repositoryId: Int): RepositoryItemDomainModel
    fun getBookMarkedRepositories(): Flow<List<RepositoryItemDomainModel>>
    suspend fun deleteBookMarkedRepository(repository: RepositoryItemDomainModel)
}