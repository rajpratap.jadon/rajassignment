package com.example.rajassginment.data.mapper

interface MapperFromEntity<Entity, Domain> {
    fun mapFromEntity(entity: Entity): Domain
}