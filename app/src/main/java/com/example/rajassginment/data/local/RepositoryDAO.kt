package com.example.rajassginment.data.local

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import kotlinx.coroutines.flow.Flow


@Dao
interface RepositoryDAO {
    @Insert(onConflict = REPLACE)
    suspend fun insert(repositoryItemDomainModel: RepositoryItemDomainModel): Long

    @Delete
    suspend fun delete(repositoryItemDomainModel: RepositoryItemDomainModel)

    @Query("SELECT * FROM REPOSITORIES_LOCAL_TABLE")
    fun getBookMarkedRepositories(): Flow<List<RepositoryItemDomainModel>>

    @Query("SELECT * FROM REPOSITORIES_LOCAL_TABLE WHERE id=:repositoryId")
    suspend fun getBookMarkedRepository(repositoryId: Int): RepositoryItemDomainModel

    @Query("SELECT id FROM REPOSITORIES_LOCAL_TABLE")
    fun getBookMarkedRepositoriesId(): Flow<List<Int>>

    @Update(onConflict = REPLACE)
    suspend fun update(repositoryItemDomainModel: RepositoryItemDomainModel)

    @Query("DELETE FROM REPOSITORIES_LOCAL_TABLE")
    suspend fun deleteTable()
}