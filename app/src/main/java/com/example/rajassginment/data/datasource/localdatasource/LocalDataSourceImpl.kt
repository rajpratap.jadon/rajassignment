package com.example.rajassginment.data.datasource.localdatasource

import com.example.rajassginment.data.local.RepositoryDAO
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalDataSourceImpl @Inject constructor(private val repositoryDAO: RepositoryDAO) :
    LocalDataSource {
    override suspend fun bookMarkedRepository(repository: RepositoryItemDomainModel) {
        repositoryDAO.insert(repository)
    }

    override fun getBookMarkedRepositoriesId(): Flow<List<Int>> =
        repositoryDAO.getBookMarkedRepositoriesId()

    override suspend fun getBookMarkedRepository(repositoryId: Int): RepositoryItemDomainModel =
        repositoryDAO.getBookMarkedRepository(repositoryId)

    override fun getBookMarkedRepositories(): Flow<List<RepositoryItemDomainModel>> =
        repositoryDAO.getBookMarkedRepositories()

    override suspend fun deleteBookMarkedRepository(repository: RepositoryItemDomainModel) =
        repositoryDAO.delete(repository)
}