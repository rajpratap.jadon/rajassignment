package com.example.rajassginment.data.remote.repo

object NetworkConstant {
    const val REPOSITORY_ID = "id"
    const val ARCHIVE_URL = "archive_url"
    const val ARCHIVE = "archived"
    const val ASSIGNEES_URL = "assignees_url"
    const val BLOB_URL = "blobs_url"
    const val BRANCH_URL = "branches_url"
    const val CLONE_URL = "clone_url"
    const val COLLABORATORS_URL = "collaborators_url"
    const val COMMENTS_URL = "comments_url"
    const val COMMITS_URL = "commits_url"
    const val COMPARE_URL = "compare_url"
    const val CONTENTS_URL = "contents_url"
    const val CONTRIBUTORS_URL = "contributors_url"
    const val CREATED_AT = "created_at"
    const val DEFAULT_BRANCH = "default_branch"
    const val DEPLOYMENTS_URL = "deployments_url"
    const val DESCRIPTION = "description"
    const val DISABLED = "disabled"
    const val DOWNLOADS_URL = "downloads_url"
    const val EVENTS_URL = "events_url"
    const val FORK = "fork"
    const val FORKS = "forks"
    const val FORKS_COUNT = "forks_count"
    const val FORKS_URL = "forks_url"
    const val FULL_NAME = "full_name"
    const val GIT_COMMITS_URL = "git_commits_url"
    const val GIT_REFS_URL = "git_refs_url"
    const val GIT_TAG_URL = "git_tags_url"
    const val GIT_URL = "git_url"
    const val HAS_DOWNLOADS = "has_downloads"
    const val HAS_ISSUES = "has_issues"
    const val HAS_PAGES = "has_pages"
    const val HAS_PROJECTS = "has_projects"
    const val HAS_WIKI = "has_wiki"
    const val HOMEPAGE = "homepage"
    const val HOOKS_URL = "hooks_url"
    const val HTML_URL = "html_url"
    const val ISSUE_COMMENT_URL = "issue_comment_url"
    const val ISSUE_EVENTS_URL = "issue_events_url"
    const val ISSUE_URL = "issues_url"
    const val KEYS_URL = "keys_url"
    const val LABELS_URL = "labels_url"
    const val LANGUAGE = "language"
    const val LANGUAGE_URL = "languages_url"
    const val LICENSE = "license"
    const val MASTER_BRANCH = "master_branch"
    const val MERGES_URL = "merges_url"
    const val MILESTONES_URL = "milestones_url"
    const val MIRROR_URL = "mirror_url"
    const val NAME = "name"
    const val NODE_ID = "node_id"
    const val NOTIFICATION_URL = "notifications_url"
    const val OPEN_ISSUES = "open_issues"
    const val OPEN_ISSUES_COUNT = "open_issues_count"
    const val OWNER = "owner"
    const val PRIVATE = "private"
    const val PULLS_URL = "pulls_url"
    const val PUSHED_AT = "pushed_at"
    const val RELEASES_URL = "releases_url"
    const val SCORE = "score"
    const val SIZE = "size"
    const val SSH_URL = "ssh_url"
    const val STARGAZERS_COUNT = "stargazers_count"
    const val STARGAZERS_URL = "stargazers_url"
    const val STATUSES_URL = "statuses_url"
    const val SUBSCRIBERS_URL = "subscribers_url"
    const val SUBSCRIPTION_URL = "subscription_url"
    const val SVN_URL = "svn_url"
    const val TAGS_URL = "tags_url"
    const val TEAMS_URL = "teams_url"
    const val TREES_URL = "trees_url"
    const val UPDATED_URL = "updated_at"
    const val URL = "url"
    const val VISIBILITY = "visibility"
    const val WATCHERS = "watchers"
    const val WATCHERS_COUNT = "watchers_count"
    const val KEY = "key"
    const val SPDX_ID = "spdx_id"
    const val AVATAR_URL = "avatar_url"
    const val FOLLOWERS_URL = "followers_url"
    const val FOLLOWING_URL = "following_url"
    const val GISTS_URL = "gists_url"
    const val GRAVATAR_ID = "gravatar_id"
    const val LOGIN = "login"
    const val ORGANIZATION_URL = "organizations_url"
    const val RECEIVED_EVENTS_URL = "received_events_url"
    const val REPO_URL = "repos_url"
    const val SITE_ADMIN = "site_admin"
    const val STARRED_URL = "starred_url"
    const val SUBSCRIPTIONS_URL = "subscriptions_url"
    const val TYPE = "type"
}