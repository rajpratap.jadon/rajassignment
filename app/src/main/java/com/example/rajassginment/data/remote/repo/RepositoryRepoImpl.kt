package com.example.rajassginment.data.remote.repo

import com.example.rajassginment.data.datasource.localdatasource.LocalDataSource
import com.example.rajassginment.data.datasource.remotedataSource.RemoteDataSource
import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.DataState
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RepositoryRepoImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) :
    RepositoryRepo {
    override fun getRepositories(query: String): Flow<DataState<BaseResponse<List<RepositoryItemDomainModel>>>> =
        remoteDataSource.getRepositories(query)

    override suspend fun bookMarkedRepository(repository: RepositoryItemDomainModel) {
        localDataSource.bookMarkedRepository(repository)
    }

    override fun getBookMarkedRepositories(): Flow<List<RepositoryItemDomainModel>> =
        localDataSource.getBookMarkedRepositories()

    override fun getBookMarkedRepositoriesId(): Flow<List<Int>> =
        localDataSource.getBookMarkedRepositoriesId()

    override suspend fun getBookMarkedRepository(repositoryId: Int) =
        localDataSource.getBookMarkedRepository(repositoryId)

    override suspend fun deleteBookMarkedRepository(repository: RepositoryItemDomainModel) {
        localDataSource.deleteBookMarkedRepository(repository)
    }
}