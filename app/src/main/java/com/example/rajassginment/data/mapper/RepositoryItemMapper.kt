package com.example.rajassginment.data.mapper

import com.example.rajassginment.common.extension.convertServerDateToAppDateString
import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import com.example.rajassginment.data.model.RepositoryItemEntity

class RepositoryItemMapper :
    MapperFromEntity<BaseResponse<List<RepositoryItemEntity>>, BaseResponse<List<RepositoryItemDomainModel>>> {
    override fun mapFromEntity(entity: BaseResponse<List<RepositoryItemEntity>>): BaseResponse<List<RepositoryItemDomainModel>> {

        val repositoryItemDomainModel = emptyList<RepositoryItemDomainModel>().toMutableList()

        entity.repositoriesItems.map {
            repositoryItemDomainModel.add(
                RepositoryItemDomainModel().apply {
                    repositoryId = it.repositoryId
                    it.fullName?.let { name -> fullName = name }
                    it.createdAt?.let { date ->
                        createdAt = date.convertServerDateToAppDateString()
                    }
                }
            )
        }

        return BaseResponse(
            incompleteResults = entity.incompleteResults,
            totalCount = entity.totalCount,
            repositoriesItems = repositoryItemDomainModel
        )
    }
}