package com.example.rajassginment.di

import android.content.Context
import androidx.room.Room
import com.example.rajassginment.data.local.AppDatabase
import com.example.rajassginment.data.local.RepositoryDAO
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppDatabaseModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase =
        Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "APP_DATABASE.db"
        ).fallbackToDestructiveMigration().build()

    @Singleton
    @Provides
    fun provideRepoDao(appDatabase: AppDatabase): RepositoryDAO {
        return appDatabase.repositoryDAO
    }
}