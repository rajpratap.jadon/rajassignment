package com.example.rajassginment.di

import com.example.rajassginment.data.datasource.localdatasource.LocalDataSource
import com.example.rajassginment.data.datasource.localdatasource.LocalDataSourceImpl
import com.example.rajassginment.data.datasource.remotedataSource.RemoteDataSource
import com.example.rajassginment.data.datasource.remotedataSource.RemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class DataSourceProvider {

    @Binds
    abstract fun bindRemoteDataSource(remoteDataSourceImpl: RemoteDataSourceImpl): RemoteDataSource

    @Binds
    abstract fun bindLocalDataSource(localDataSourceImpl: LocalDataSourceImpl): LocalDataSource
}