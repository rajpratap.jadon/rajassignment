package com.example.rajassginment.di

import com.example.rajassginment.data.mapper.MapperFromEntity
import com.example.rajassginment.data.mapper.RepositoryItemMapper
import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import com.example.rajassginment.data.model.RepositoryItemEntity
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MapperProvider {

    @Singleton
    @Provides
    fun providesRepositoryMapper(): MapperFromEntity<BaseResponse<List<RepositoryItemEntity>>, BaseResponse<List<RepositoryItemDomainModel>>> =
        RepositoryItemMapper()
}