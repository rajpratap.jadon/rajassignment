package com.example.rajassginment.di

import com.example.rajassginment.data.remote.repo.RepositoryRepo
import com.example.rajassginment.data.remote.repo.RepositoryRepoImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepoProvider {
    @Binds
    abstract fun bindsRepositoryRepo(
        repositoryRepoImpl: RepositoryRepoImpl
    ): RepositoryRepo
}