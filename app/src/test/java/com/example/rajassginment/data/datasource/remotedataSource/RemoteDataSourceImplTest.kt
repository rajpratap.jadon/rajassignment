package com.example.rajassginment.data.datasource.remotedataSource

import com.example.rajassginment.data.model.BaseResponse
import com.example.rajassginment.data.model.DataState
import com.example.rajassginment.data.model.RepositoryItemDomainModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RemoteDataSourceImplTest : RemoteDataSource {

    private val repoItem = mutableListOf<RepositoryItemDomainModel>()

    override fun getRepositories(query: String): Flow<DataState<BaseResponse<List<RepositoryItemDomainModel>>>> =
        flow {
            emit(DataState.Loading)
            emit(
                DataState.Success(
                    BaseResponse(
                        incompleteResults = true,
                        totalCount = 13227083,
                        repositoriesItems = repoItem
                    )
                )
            )
        }
}