package com.example.rajassginment.common.extension

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class StringExtensionKtTest {

    @Test
    fun convertServerDateToAppDateString() {
        val utcDate = "2012-06-05T19:34:12Z"
        var formatDate = "06 Jun 2012, 01:04 AM"
        var result = utcDate.convertServerDateToAppDateString()
        assertThat(result).isEqualTo(formatDate)

        formatDate = "06 Jun 2012, 01:04 PM"
        result = utcDate.convertServerDateToAppDateString()
        assert(result != formatDate)
    }
}